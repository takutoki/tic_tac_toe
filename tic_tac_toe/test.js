var $id = function(id) {
	return document.getElementById(id);
};


// マウス座標
var mouse = {
	x : null,
	y : null
};

// 描画位置
var pos = {
	x : null,
	y : null
}

// 同じ位置をクリックしたとき描画を止める作戦
//var check = null;

// 同じ位置をクリックしたとき置けなくする作戦
var pos_flag = {
	// 上の列
	aa : false,
	ab : false,
	ac : false,
	// 真ん中
	ba : false,
	bb : false,
	bc : false,
	// 下の列
	ca : false,
	cb : false,
	cc : false
}

var context = null; // コンテキスト
var canvas = null; // Canvas
var change_flag = true; // ○×の切り替え
var finish = 0; // 終了判定

window.onload = function() {
	canvas = $id("canvas");
	context = canvas.getContext('2d');

	draw();
};

/**
 * マウス位置を取得
 */
var getMousePosOnElement = function(e) {

	var rect = e.target.getBoundingClientRect();
	mouse.x = e.clientX - rect.left;
	mouse.y = e.clientY - rect.top;

	return mouse;
};

/**
 * 終了判定 ○×の合計が9個になったら終了
 */
function count() {
	if (finish > 8) {
		context.fillText("おわる ", 8, 600);
		alert("おわり");
	}
};

/**
 * 描画処理
 */
function draw() {

	if (canvas.getContext) {

		// クリア
		context.clearRect(0, 0, canvas.width, canvas.height);

		context.fillStyle = "rgba(0, 0, 0, 1.0)";

		/**
		 * ボードの描画
		 */
		// イメージオブジェクトの生成
		var board = new Image();
		board.src = "resource/board.png";

		// 画像が表示されるのを待ってから処理を実行
		board.onload = function() {
			// 画像を描画する
			context.drawImage(board, 15, 15);
		};

		/**
		 * クリックしたときの処理
		 */
		canvas.addEventListener('click', function(event) {
			if (finish < 9) {
				// マウス位置を更新
				getMousePosOnElement(event);

				// 同じマスに置けなくするやつ
				var check = false;

				// マスの範囲外だったら置けなくする
				if (mouse.x > 15 && mouse.x < 465 && mouse.y > 15
						&& mouse.y < 465) {

					/**
					 * マスの判定 クリックしたマスの真ん中に画像を表示させる
					 */
					// 上の左
					if (!pos_flag.aa) {
						if (mouse.x > 15 && mouse.x < 163 && mouse.y > 15
								&& mouse.y < 163) {
							pos.x = 15;
							pos.y = 15;
							pos_flag.aa = true;
							check = true;
						}
					}

					// 上の真ん中
					if (!pos_flag.ab) {
						if (mouse.x > 164 && mouse.x < 313 && mouse.y > 15
								&& mouse.y < 163) {
							pos.x = 165;
							pos.y = 15;
							pos_flag.ab = true;
							check = true;
						}
					}

					// 上の右
					if (!pos_flag.ac) {
						if (mouse.x > 314 && mouse.x < 463 && mouse.y > 15
								&& mouse.y < 163) {
							pos.x = 315;
							pos.y = 15;
							pos_flag.ac = true;
							check = true;
						}
					}

					// 真ん中の左
					if (!pos_flag.ba) {
						if (mouse.x > 15 && mouse.x < 163 && mouse.y > 164
								&& mouse.y < 313) {
							pos.x = 15;
							pos.y = 165;
							pos_flag.ba = true;
							check = true;
						}
					}

					// 真ん中の真ん中
					if (!pos_flag.bb) {
						if (mouse.x > 164 && mouse.x < 313 && mouse.y > 164
								&& mouse.y < 313) {
							pos.x = 165;
							pos.y = 165;
							pos_flag.bb = true;
							check = true;
						}
					}

					// 真ん中の右
					if (!pos_flag.bc) {
						if (mouse.x > 314 && mouse.x < 463 && mouse.y > 164
								&& mouse.y < 313) {
							pos.x = 315;
							pos.y = 165;
							pos_flag.bc = true;
							check = true;
						}
					}

					// 下の左
					if (!pos_flag.ca) {
						if (mouse.x > 15 && mouse.x < 163 && mouse.y > 314
								&& mouse.y < 463) {
							pos.x = 15;
							pos.y = 315;
							pos_flag.ca = true;
							check = true;
						}
					}

					// 下の左
					if (!pos_flag.cb) {
						if (mouse.x > 164 && mouse.x < 313 && mouse.y > 314
								&& mouse.y < 463) {
							pos.x = 165;
							pos.y = 315;
							pos_flag.cb = true;
							check = true;
						}
					}

					// 下の左
					if (!pos_flag.cc) {
						if (mouse.x > 314 && mouse.x < 463 && mouse.y > 314
								&& mouse.y < 463) {
							pos.x = 315;
							pos.y = 315;
							pos_flag.cc = true;
							check = true;
						}
					}

					// 置けなかったとき入らないようにする
					if (check) {
						// change_flagがtrueだったら○が置ける
						// falseだったら×が置ける
						if (change_flag == true) {

							/**
							 * circleの描画
							 */
							// イメージオブジェクトの生成
							var circle = new Image();
							circle.src = "resource/circle.png";

							// 画像が表示されるのを待ってから処理を実行
							circle.onload = function() {
								// 画像を描画する
								context.drawImage(circle, pos.x, pos.y);

								// ×に切り替え
								change_flag = false;

								count();

							};

						} else {

							/**
							 * crossの描画
							 */
							// イメージオブジェクトの生成
							var cross = new Image();
							cross.src = "resource/cross.png";

							// 画像が表示されるのを待ってから処理を実行
							cross.onload = function() {
								// 画像を描画する
								context.drawImage(cross, pos.x, pos.y);

								// ○に切り替え
								change_flag = true;

								count();

							};
						}
					}

				}
			}

			// 置けなかったとき入らないようにする
			if (check) {
				// マスの範囲外だったらカウントしなくする
				if (mouse.x > 15 && mouse.x < 465 && mouse.y > 15
						&& mouse.y < 465) {
					// ○か×を置いたら＋1する
					finish++;

					context.fillText(finish, 8, 550);
				}
			}
		}, false);

	}
}
